package com.cosc431.tipcalculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Double.parseDouble
import java.lang.NumberFormatException
import android.text.Editable
import android.text.TextWatcher



class MainActivity : AppCompatActivity() {

    val checkAmountWatcher: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable) {}

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            Handler().postDelayed({
                updateCheckTotalText()
            },700)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        calc_tip_btn.setOnClickListener { updateCheckTotalText() }
        tip_percent_radio_group.setOnCheckedChangeListener { _, _ -> updateCheckTotalText() }
        check_input.addTextChangedListener(checkAmountWatcher)
    }

    private fun updateCheckTotalText(){
        val checkText = check_input.text.toString();
        if(!checkText.isNullOrEmpty()) {
            try {
                val checkAmount = parseDouble(checkText);
                val tip = calcTip(checkAmount);
                val total = calcTotal(checkAmount);
                val formattedTip = "%.2f".format(tip);
                val formattedTotal = "%.2f".format(total);
                val textDisplay = "Your calculated tip is $$formattedTip and your total is $$formattedTotal"
                tip_and_total_txt.setText(textDisplay)
            } catch (e: NumberFormatException) {
                Log.d("Invalid value", checkText)
                resetCheckTotalText()
            }
        }else{
            resetCheckTotalText();
        }
    }
    private fun resetCheckTotalText(){
        tip_and_total_txt.setText("")
    }

   private fun calcTip(checkAmount: Double): Double{
        val percentage = getTipPercentage();
        return checkAmount * percentage;
    }

    private fun getTipPercentage(): Double{
        val selected = tip_percent_radio_group.checkedRadioButtonId;
        when(selected){
            ten_percent_radio.id -> return  .10;
            twnty_percent_radio.id -> return  .20;
            thrty_percent_radio.id -> return  .30;
            else -> return 1.0;
        }
    }

    private fun calcTotal(checkAmount: Double): Double{
        return checkAmount + calcTip(checkAmount);
    }
}
